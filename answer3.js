//3. หาผลรวมของ qty ของ items ของลูกค้านั้นๆ
const datas = require('./datas.json')

let qty_fname = {}

datas.forEach((customer) => {
    const sumQty = customer.items.reduce((accItems, product) => accItems + product.qty, 0)
    qty_fname[customer.firstName] = sumQty
})

console.log(qty_fname)

