//หาจำนวนผู้ที่มี member และเอา  คะแนน member มารวมกันเป็นอันเดียว
// Output { 
//     memberCount: 10,
//     score: 1000}
const datas = require('./datas.json')

let output ={}
var sumMember = 0
var sumScore = 0

datas.forEach((customer) => {
    if (customer.member !== undefined) {
        sumMember++;
        sumScore += Number(customer.member.score);
    }
});
output["memberCount"] = sumMember;
output["score"] = sumScore;
console.log(output);
// score: good

// better version
const result = datas.reduce((acc, item) => {
	if (item.member) {
		acc.memberCount++
		acc.score += +item.member.score
	}
	// string to number cant write this +item.member.score, Number(item.member.score), parseInt(item.member.score)
	return acc
}, {
	memberCount: 0,
	score: 0
})
console.log('result: ', result)