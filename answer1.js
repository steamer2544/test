// 1. หาจำนวน female & male ว่ามีทั้งหมดกี่รายการ output { female: 0, male: 0,}
const datas = require('./datas.json')

const sex = { 
    female: datas.filter(data => data.sex === "female").length,
    male: datas.filter(data => data.sex === "male").length
}
console.log(sex);
// score: good

// better version
const result = datas.reduce((acc, item) => {
	acc[item.sex] += 1
	return acc
}, {
	female: 0,
	male: 0
})
console.log('result: ', result)

// test push